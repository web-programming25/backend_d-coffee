import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import OrderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Product from "@/types/Product";
import product from "@/services/product";
import auth from "@/services/auth";
import { useAuthStore } from "./auth";

export const useOrderStore = defineStore("Order", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const dialog = ref(false);
  const Orders = ref<Order[]>([]);
  const editedOrder = ref<Order>({});

  const orderList = ref<{ product: Product; amount: number; sum: number }[]>(
    []
  );
  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, sum: 1 * item.price });
  }
  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }

  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });

  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedOrder.value = {};
    }
  });
  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.getOrder();
      Orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function openOrder() {
    console.log(authStore.getUser());
  }
  //   loadingStore.isLoading = true;
  //   const orderItem = orderList.value.map(
  //     (item) =>
  //       <{ productId: number; amount: number }>{
  //         productId: item.product.id,
  //         amount: item.amount,
  //       }
  //   );
  //   const order = { userId: User.id, orderItem: orderItem };
  //   try {
  //     const res = await OrderService.saveOrder(order);

  //     dialog.value = false;
  //     await getOrders();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึก Order ได้");
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }

  async function deleteOrder(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.deleteOrder(id);
      await getOrders();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Order ได้");
    }
    loadingStore.isLoading = false;
  }
  function editOrder(Order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(Order));
    dialog.value = true;
  }
  return {
    Orders,
    getOrders,
    dialog,
    editedOrder,
    openOrder,
    editOrder,
    deleteOrder,
    addProduct,
    deleteProduct,
    sumAmount,
    sumPrice,
    orderList,
  };
});
