import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  productId?: number;
  product?: Product;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  order?: Order;
}
