export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  point: number;
  age: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
